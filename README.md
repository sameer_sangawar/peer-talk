# README #

### What is PeerTalk? ###
* PeerTalk Android application allows users on the network to chat with each other
* The system uses web sockets for communication and therefore is extensible to other platforms like iOS, Windows etc


### How to Install PeerTalk? ###
### ANDROID ###
* To get started, download and install the application from the APK URL below on any Android device running KitKat and above
* https://bitbucket.org/sameer_sangawar/peer-talk/raw/260ac16955759fcfb32e7ec28194383ce1596cb9/apk/peertalk.apk

### WEB INTERFACE ###
* A rudimentary web interface for the chat is available at http://54.255.133.164:9000
* Enter a screen name and click on “Connect” to join PeerTalk
* The left panel displays all online peers. Click on any one (does not get highlighted) and type in your message in the input box and click on Send


### Details of PeerTalk ###

### ANDROID APPLICATION ###
* Users must be online to talk to peers that are online as well
* All online peers are listed in the PEERS tab
* All previous and current chats are listed in the HISTORY tab. The availability status (online or offline) is indicated against each peer in the list. The last message exchanged is also shown for each peer
* Chat history is available to the users in both online and offline modes.
* In the individual peer chat screen, user can see when his peer starts “typing” in the action bar under the peer name.

### SERVER ###
* The server is an Amazon EC2 micro instance running a Play Framework application which listens to incoming web socket connections on port 9000
* The application is Java based and uses Java WebSockets
* Currently there is no database on this server and all information is available as long as the server is up & running


### Known Issues ###
* The application currently does not handle scenarios when a user goes offline while using the app in foreground or the background.