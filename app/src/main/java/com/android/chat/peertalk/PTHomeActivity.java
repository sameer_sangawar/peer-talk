package com.android.chat.peertalk;

import android.content.Context;
import android.os.AsyncTask;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.os.Bundle;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.android.chat.peertalk.chat.PTChatPeersFragment;
import com.android.chat.peertalk.chat.PTChatHistoryFragment;
import com.android.chat.peertalk.entities.PTPeer;
import com.android.chat.peertalk.login.PTLoginFragment;
import com.android.chat.peertalk.manager.PTChatManager;
import com.android.chat.peertalk.manager.PTDBManager;
import com.android.chat.peertalk.manager.PTUserManager;
import com.android.chat.peertalk.utils.PTConstants;

public class PTHomeActivity extends FragmentActivity implements PTLoginFragment.PTLoginInterface,
        PTChatPeersFragment.PTPeerInterface, PTFragmentReadyInterface {

    private PTApplication application = null;
    private PTChatManager chatManager = null;
    private PTLoginFragment loginFragment = null;
    private PTChatHistoryFragment chatHistoryFragment = null;
    private PTChatPeersFragment chatPeersFragment = null;
    private ViewPager homeViewPager = null;
    private FrameLayout loginContainer = null;
    private PTHomePagerAdapter homePagerAdapter = null;
    private TextView historyTabTV = null;
    private TextView peersTabTV = null;
    private LinearLayout appErrorLL = null;
    private boolean isChatPeersReady = false;
    private boolean isChatHistoryReady = false;
    public static final int TAB_HISTORY = 0;
    public static final int TAB_PEERS = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        application = (PTApplication)getApplication();
        loginContainer = (FrameLayout) findViewById(R.id.loginContainer);
        historyTabTV = (TextView) findViewById(R.id.historyTabTV);
        historyTabTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                homeViewPager.setCurrentItem(TAB_HISTORY);
                setActiveTab(TAB_HISTORY);
            }
        });

        peersTabTV = (TextView) findViewById(R.id.peersTabTV);
        peersTabTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                homeViewPager.setCurrentItem(TAB_PEERS);
                setActiveTab(TAB_PEERS);
            }
        });

        homeViewPager = (ViewPager) findViewById(R.id.homeViewPager);
        homeViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                setActiveTab(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        appErrorLL = (LinearLayout) findViewById(R.id.appErrorLL) ;
        appErrorLL.setVisibility(View.GONE);

        //Verify if database exists, if not create it
        VerifyAndCreateDatabaseTask dbTask = new VerifyAndCreateDatabaseTask(this);
        dbTask.execute();
    }

    public void initPeerTalk() {
        PTUserManager userManager = PTUserManager.getInstance(this);
        chatManager = PTChatManager.getInstance(this);
        if(userManager.isLoggedIn()) {
            loginContainer.setVisibility(View.GONE);
            chatManager.initialize(application);
            displayLists();
        }
        else {
            if(loginFragment == null){
                loginFragment = new PTLoginFragment();
            }
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.loginContainer,loginFragment,"login").commit();
            loginContainer.setVisibility(View.VISIBLE);
        }
    }

    /**
     *
     * @param fragment
     *
     * We request a web socket connection only after both fragments have been instantiated
     * and have the callback listeners setup
     *
     */
    synchronized public void onFragmentReady(Fragment fragment) {
        if(fragment instanceof PTChatPeersFragment) {
            isChatPeersReady = true;
        }
        else if(fragment instanceof PTChatHistoryFragment) {
            isChatHistoryReady = true;
        }

        if(isChatPeersReady && isChatHistoryReady) {
            chatManager.connect();
        }
    }

    /**
     * Setup the ViewPager & Adapter to display Peers & History fragments
     */
    private void displayLists() {
        if(homePagerAdapter == null) {
            homePagerAdapter = new PTHomePagerAdapter(getSupportFragmentManager());
        }
        homeViewPager.setAdapter(homePagerAdapter);
        homeViewPager.setOffscreenPageLimit(2);

        loginContainer.setVisibility(View.GONE);
    }

    /**
     * Callback method on successful login.
     */
    @Override
    public void onLoginSuccess() {
        initPeerTalk();
    }

    /**
     *
     * @param peer
     *
     * Callback method when a peer is selected/tapped in the peers fragment.
     * This will add the peer to the history and initiate a chat session
     *
     */
    @Override
    public void onPeerSelected(PTPeer peer) {
        if(chatHistoryFragment != null) {
            chatHistoryFragment.addPeer(peer);
        }
    }

    private void setActiveTab(int pos) {
        switch (pos) {
            case TAB_HISTORY:
                historyTabTV.setTextColor(ContextCompat.getColor(this,R.color.white));
                peersTabTV.setTextColor(ContextCompat.getColor(this,R.color.white_75_trans));
                break;

            case TAB_PEERS:
                peersTabTV.setTextColor(ContextCompat.getColor(this,R.color.white));
                historyTabTV.setTextColor(ContextCompat.getColor(this,R.color.white_75_trans));
                break;
        }
    }

    private class PTHomePagerAdapter extends FragmentPagerAdapter {

        public PTHomePagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case TAB_HISTORY:
                    if(chatHistoryFragment == null) {
                        chatHistoryFragment = new PTChatHistoryFragment();
                    }
                    return chatHistoryFragment;

                case TAB_PEERS:
                    if(chatPeersFragment == null) {
                        chatPeersFragment = new PTChatPeersFragment();
                    }
                    return chatPeersFragment;

            }
            return null;
        }

        @Override
        public int getCount() {
            return 2;
        }
    }

    // AsyncTask to create and verify Database on App Launch
    private class VerifyAndCreateDatabaseTask extends AsyncTask<Void, Void, Boolean> {
        private Context ctx;

        public VerifyAndCreateDatabaseTask(Context c) {
            ctx = c;
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            Boolean dbAvailable;
            try {
                PTDBManager dbHelper = PTDBManager.getInstance(ctx);
                dbHelper.createDataBase();
                dbHelper.openDataBase();
                dbAvailable = true;
            }
            catch (Exception e) {
                dbAvailable = false;
                if(PTConstants.DUMP_TRACE){
                    e.printStackTrace();
                }
            }

            return dbAvailable;
        }

        @Override
        protected void onPostExecute(Boolean dbAvailable) {
            if(dbAvailable) {
                initPeerTalk();
            }
            else {
                appErrorLL.setVisibility(View.VISIBLE);
            }
        }
    }
}
