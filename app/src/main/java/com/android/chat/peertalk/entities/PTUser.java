package com.android.chat.peertalk.entities;

/**
 * Created by ssangawar on 26/10/2016.
 */

public class PTUser {
    public long userId = -1;
    public String userName = "";
    public String userPassword = "";
    public String userInitials = "";
    public String userImageUrl = "";
    public String userStatus = "";
    public int userPresence;
}
