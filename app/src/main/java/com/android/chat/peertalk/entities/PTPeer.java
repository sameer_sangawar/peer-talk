package com.android.chat.peertalk.entities;

import com.android.chat.peertalk.utils.PTConstants;

/**
 * Created by sameerss on 26/10/16.
 */

public class PTPeer {

    public long peerId = -1;
    public String peerName = "";
    public String peerInitials = "";
    public String peerImageUrl = "";
    public String peerStatus = "";
    public int peerPresence = PTConstants.PEER_PRESENCE_OFFLINE;
    public String lastMessage = "";
    public String lastMessageTime = "";
    public int lastMessageType;
    public boolean lastMessageReadStatus = false;
    public int unreadMessageCount = 0;

}
