package com.android.chat.peertalk.chat;

import com.android.chat.peertalk.entities.PTMessage;

/**
 * Created by ssangawar on 27/10/2016.
 */

public interface PTChatMessageInterface {
    void onGotMessage(PTMessage message);
}
