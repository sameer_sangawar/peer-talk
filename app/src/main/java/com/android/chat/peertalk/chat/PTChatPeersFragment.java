package com.android.chat.peertalk.chat;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.chat.peertalk.PTApplication;
import com.android.chat.peertalk.PTFragmentReadyInterface;
import com.android.chat.peertalk.R;
import com.android.chat.peertalk.entities.PTMessage;
import com.android.chat.peertalk.entities.PTPeer;
import com.android.chat.peertalk.manager.PTChatManager;
import com.android.chat.peertalk.utils.PTConstants;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by sameerss on 26/10/16.
 */

public class PTChatPeersFragment extends Fragment implements PTChatPeersInterface, PTChatMessageInterface {

    private LinearLayout joinLL;
    private RecyclerView peersRV;
    private ProgressBar peerPB;
    private TextView noPeersTV;
    private TextView joinTV;

    private PTApplication application;
    private PTPeersAdapter peersAdapter;
    private PTChatManager chatManager;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_chat_peers,container,false);
        peersRV = (RecyclerView) view.findViewById(R.id.peersRV);
        peerPB = (ProgressBar) view.findViewById(R.id.peerPB);
        noPeersTV = (TextView) view.findViewById(R.id.noPeersTV);
        joinTV = (TextView) view.findViewById(R.id.joinTV);
        joinLL = (LinearLayout) view.findViewById(R.id.joinLL);
        return view;
    }

    @Override
    public void onActivityCreated(Bundle bundle) {
        super.onActivityCreated(bundle);

        application = (PTApplication)getActivity().getApplication();
        chatManager = PTChatManager.getInstance(getActivity());
        chatManager.setPeerListener(this);
        chatManager.setMessageListener(this);
        peersRV.setLayoutManager(new LinearLayoutManager(getActivity()));
        peersAdapter = new PTPeersAdapter();
        peersRV.setAdapter(peersAdapter);
        joinLL.setVisibility(View.GONE);
        joinTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(application.isOnline()) {
                    peerPB.setVisibility(View.VISIBLE);
                    joinLL.setVisibility(View.GONE);
                    if(!chatManager.reConnect(application)) {
                        peerPB.setVisibility(View.VISIBLE);
                        joinLL.setVisibility(View.GONE);
                    }
                }
                else {
                    Toast.makeText(getActivity(),R.string.online,Toast.LENGTH_LONG).show();
                    peerPB.setVisibility(View.GONE);
                }
            }
        });

        if(application.isOnline()) {
            peerPB.setVisibility(View.VISIBLE);
        }
        else {
            Toast.makeText(getActivity(),R.string.online,Toast.LENGTH_LONG).show();
            peerPB.setVisibility(View.GONE);
        }

        ((PTFragmentReadyInterface)getActivity()).onFragmentReady(PTChatPeersFragment.this);
    }

    @Override
    public void onResume(){
        super.onResume();
    }

    public void onDestroy() {
        chatManager.close();
        super.onDestroy();
    }

    public void onGotMessage(PTMessage message) {
        if(message.messageType == PTConstants.MSG_TYPE_CONNECT_ERROR) {

            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    peerPB.setVisibility(View.GONE);
                    joinLL.setVisibility(View.VISIBLE);
                    Toast.makeText(getActivity(),R.string.connect_error,Toast.LENGTH_LONG).show();
                }
            });

        }
    }

    public void onGotPeers(final ArrayList<PTPeer> peers) {
        if(peers != null) {

            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    peerPB.setVisibility(View.GONE);
                    if(peers.isEmpty()) {
                        noPeersTV.setVisibility(View.VISIBLE);
                    }
                    else {
                        noPeersTV.setVisibility(View.GONE);
                    }
                    peersAdapter.updateDataSet(peers);
                }
            });
        }
    }

    public void onPeerJoined(PTPeer peer) {
        peersAdapter.addPeer(peer);

    }

    public void onPeerLeft(PTPeer peer) {
        peersAdapter.removePeer(peer);
    }

    private class PTPeersAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

        private static final int VIEW_TYPE_PEER_ROW = 1;
        public HashMap<String ,PTPeer> peers;
        public ArrayList<String> peerKeys;
        private int[] colors = new int[5];

        public PTPeersAdapter() {
            colors[0] = ContextCompat.getColor(getActivity(),R.color.peer_row_initials_bg_1);
            colors[1] = ContextCompat.getColor(getActivity(),R.color.peer_row_initials_bg_2);
            colors[2] = ContextCompat.getColor(getActivity(),R.color.peer_row_initials_bg_3);
            colors[3] = ContextCompat.getColor(getActivity(),R.color.peer_row_initials_bg_4);
            colors[4] = ContextCompat.getColor(getActivity(),R.color.peer_row_initials_bg_5);
            peers = chatManager.peers;
            peerKeys = chatManager.peersKeys;
        }

        private class PTPeerViewHolder extends RecyclerView.ViewHolder {
            private ImageView peerIV;
            private TextView peerInitialsTV;
            private TextView peerNameTV;

            private PTPeerViewHolder(View view) {
                super(view);

                peerIV = (ImageView) view.findViewById(R.id.peerIV);
                peerInitialsTV = (TextView) view.findViewById(R.id.peerInitialsTV);
                peerNameTV = (TextView) view.findViewById(R.id.peerNameTV);
            }
        }

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            RecyclerView.ViewHolder viewHolder = null;
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());

            switch (viewType) {
                case VIEW_TYPE_PEER_ROW:
                    View view = inflater.inflate(R.layout.peer_row,parent,false);
                    viewHolder = new PTPeerViewHolder(view);
                    break;
            }

            return viewHolder;
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
            final PTPeer peer = peers.get(peerKeys.get(position));
            switch (holder.getItemViewType()) {
                case VIEW_TYPE_PEER_ROW:
                    PTPeerViewHolder peerViewHolder = (PTPeerViewHolder)holder;
                    peerViewHolder.peerNameTV.setText(peer.peerName);
                    peerViewHolder.peerInitialsTV.setText(peer.peerInitials.toUpperCase());
                    peerViewHolder.peerInitialsTV.setBackgroundColor(colors[position%colors.length]);
                    break;
            }

            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    chatManager.currentPeer = peer;
                    ((PTPeerInterface)getActivity()).onPeerSelected(peer);
                    Intent peerChatIntent = new Intent(getActivity(),PTPeerChatActivity.class);
                    peerChatIntent.putExtra("peerId",peer.peerId);
                    startActivity(peerChatIntent);
                }
            });
        }

        @Override
        public int getItemViewType(int position) {
            return VIEW_TYPE_PEER_ROW;
        }

        @Override
        public int getItemCount() {
            return peers.size();
        }

        public void updateDataSet(ArrayList<PTPeer> newPeers) {
            if(null != getActivity()) {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        notifyDataSetChanged();
                    }
                });
            }
        }

        public void addPeer(PTPeer newPeer) {
            if(null != getActivity()) {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        noPeersTV.setVisibility(View.GONE);
                        notifyDataSetChanged();
                    }
                });
            }
        }

        public void removePeer(PTPeer peer) {
            if(null != getActivity()) {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if(peers.isEmpty()) {
                            noPeersTV.setVisibility(View.VISIBLE);
                        }
                        notifyDataSetChanged();
                    }
                });
            }
        }
    }

    public interface PTPeerInterface {
        void onPeerSelected(PTPeer peer);
    }
}
