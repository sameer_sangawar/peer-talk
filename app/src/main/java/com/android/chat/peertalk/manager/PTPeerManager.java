package com.android.chat.peertalk.manager;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.util.Log;

import com.android.chat.peertalk.entities.PTPeer;
import com.android.chat.peertalk.utils.PTConstants;

import java.util.ArrayList;

/**
 * Created by ssangawar on 27/10/2016.
 */

public class PTPeerManager {

    private static PTPeerManager instance = null;
    private Context context;
    private PTDBManager dbManager = null;

    public static PTPeerManager getInstance(Context ctx) {
        if(instance == null) {
            instance = new PTPeerManager(ctx);
        }
        return instance;
    }

    private PTPeerManager(Context ctx) {
        context = ctx;
        dbManager = PTDBManager.getInstance(ctx);
    }

    public boolean addPeer(PTPeer peer) {

        ContentValues values = new ContentValues();
        values.put("peer_id",peer.peerId);
        values.put("peer_name",peer.peerName);
        values.put("peer_initials",peer.peerInitials);
        values.put("peer_image_url",peer.peerImageUrl);
        values.put("peer_status",peer.peerStatus);
        values.put("peer_presence",peer.peerPresence);
        values.put("last_message",peer.lastMessage);
        values.put("last_message_time",peer.lastMessageTime);
        values.put("last_message_type",peer.lastMessageType);
        values.put("last_message_read_status",peer.lastMessageReadStatus);
        values.put("unread_message_count",peer.unreadMessageCount);

        try {
            dbManager.insert("peers",values);
        }
        catch (SQLException sqle) {
            if(PTConstants.DUMP_TRACE) {
                Log.e("CMGR","UNABLE TO ADD PEER",sqle);
            }

            return false;
        }

        return true;
    }

    public ArrayList<PTPeer> loadPeers() {
        ArrayList<PTPeer> peers = new ArrayList<>();

        String sql = "select * from peers";
        Cursor cursor = dbManager.query(sql);
        if(cursor != null && cursor.getCount() > 0) {

            int col_peer_id = cursor.getColumnIndex("peer_id");
            int col_peer_name = cursor.getColumnIndex("peer_name");
            int col_peer_initials = cursor.getColumnIndex("peer_initials");
            int col_peer_image_url = cursor.getColumnIndex("peer_image_url");
            int col_peer_status = cursor.getColumnIndex("peer_status");
            int col_peer_presence = cursor.getColumnIndex("peer_presence");
            int col_last_message = cursor.getColumnIndex("last_message");
            int col_last_message_time = cursor.getColumnIndex("last_message_time");
            int col_last_message_type = cursor.getColumnIndex("last_message_type");
            int col_last_message_read_status = cursor.getColumnIndex("last_message_read_status");
            int col_unread_message_count = cursor.getColumnIndex("unread_message_count");

            cursor.moveToFirst();

            do {
                PTPeer peer = new PTPeer();
                peer.peerId = cursor.getLong(col_peer_id);
                peer.peerName = cursor.getString(col_peer_name);
                peer.peerInitials = cursor.getString(col_peer_initials);
                peer.peerImageUrl = cursor.getString(col_peer_image_url);
                peer.peerStatus = cursor.getString(col_peer_status);
                peer.peerPresence = cursor.getInt(col_peer_presence);
                peer.lastMessage = cursor.getString(col_last_message);
                peer.lastMessageTime = cursor.getString(col_last_message_time);
                peer.lastMessageType = cursor.getInt(col_last_message_type);
                int status = cursor.getInt(col_last_message_read_status);
                if(status == 1) {
                    peer.lastMessageReadStatus = true;
                }
                else {
                    peer.lastMessageReadStatus = false;
                }
                peer.unreadMessageCount = cursor.getInt(col_unread_message_count);

                peers.add(peer);

            }while(cursor.moveToNext());
        }

        return peers;
    }
}
