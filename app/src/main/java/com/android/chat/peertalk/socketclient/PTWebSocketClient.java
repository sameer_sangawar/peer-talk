package com.android.chat.peertalk.socketclient;

import android.content.Context;
import android.os.Handler;
import android.util.Log;

import com.android.chat.peertalk.entities.PTMessage;
import com.android.chat.peertalk.entities.PTUser;
import com.android.chat.peertalk.manager.PTUserManager;
import com.android.chat.peertalk.utils.PTConstants;
import com.android.chat.peertalk.utils.PTUtils;

import org.java_websocket.client.WebSocketClient;
import org.java_websocket.drafts.Draft;
import org.java_websocket.drafts.Draft_17;
import org.java_websocket.handshake.ServerHandshake;
import org.json.JSONObject;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Map;

/**
 * Created by sameerss on 26/10/16.
 *
 * PTWebSocketClient extends WebSocketClient to initiate and manage
 * communications over web sockets. This is a singleton and all messages
 * are handled by this class.
 *
 * Subscriber setup a callback listener and is notified of web socket events
 * through methods as declared in PTWebSocketClientInterface
 *
 */

public class PTWebSocketClient extends WebSocketClient {

    private static PTWebSocketClient instance = null;
    private PTWebSocketClientInterface socketListener = null;

    private Handler mHandler = new Handler();
    private static final long PING_INTERVAL_TIME = 30000;

    public static PTWebSocketClient getInstance(Context ctx) {
        if(instance == null || instance.getConnection().isClosed()) {
            try {
                String userName = PTUserManager.getInstance(ctx).user.userName;
                instance = new PTWebSocketClient(new URI(PTConstants.PT_WEBSOCKET_CLIENT_URL+userName),new Draft_17(),null,30000);
            }
            catch (URISyntaxException use) {
                instance = null;
                Log.e("WEBSOCK","SOCKET CONNECTION FAILED, INVALID URI",use);
            }
            catch (Exception e) {
                e.printStackTrace();
            }
        }
        return instance;
    }

    private PTWebSocketClient(URI uri, Draft protocolDraft, Map<String,String> headers, int timeout) {
        super(uri, protocolDraft, headers, timeout);
    }

    public void ptConnect() {
        if(instance != null && !instance.getConnection().isOpen()) {
            instance.connect();
        }
    }

    public void ptClose() {
        if(instance != null && instance.getConnection().isOpen()) {
            instance.close();
            instance = null;
        }
    }

    public void setWebSocketListener(PTWebSocketClientInterface listener) {
        socketListener = listener;
    }

    @Override
    public void onOpen(ServerHandshake handshakedata) {
        socketListener.onWebSocketOpen();
    }

    @Override
    public void onMessage(String message) {
        socketListener.onWebSocketMessage(message);
    }

    @Override
    public void onClose(int code, String reason, boolean remote) {
        socketListener.onWebSocketClose(code, reason, remote);
    }

    @Override
    public void onError(Exception ex) {
        instance = null;
        socketListener.onWebSocketError(ex);
    }

    /**
     * Sets up timer to periodically send ping/connection alive message
     */
    public  void setPingTimer() {
        new Thread(new Runnable() {
            @Override
            public void run() {

                while (true) {
                    try {
                        Thread.sleep(PING_INTERVAL_TIME);
                        mHandler.post(new Runnable() {

                            @Override
                            public void run() {
                                if(instance != null && instance.getConnection().isOpen()) {
                                    pingServer();
                                }
                            }
                        });
                    } catch (Exception e) {
                        if(PTConstants.DUMP_TRACE) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        }).start();
    }


    /**
     * Sends the server a ping/connection alive message
     */
    public void pingServer() {

        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("messageType", PTConstants.MSG_TYPE_PING);
            String str = jsonObject.toString();
            instance.send(str);
        }
        catch (Exception e) {
            if(PTConstants.DUMP_TRACE) {
                e.printStackTrace();
            }
        }
    }

    /**
     *
     * @param message
     *
     * Sends a message to the server over web socket connection
     */
    public void sendMessage(PTMessage message){
        try {
            JSONObject json = new JSONObject();
            json.put("messageType" , message.messageType);
            json.put("messageFrom", message.messageFrom);
            json.put("messageFromName", message.messageFromName);
            json.put("messageTo", message.messageTo);
            json.put("messageTime", PTUtils.getDateTime());
            json.put("messageText", message.messageText);

            String str = json.toString();
            instance.send(str);

        }
        catch (Exception e) {
            if(PTConstants.DUMP_TRACE) {
                e.printStackTrace();
            }
        }
    }

}
