package com.android.chat.peertalk.chat;

import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.chat.peertalk.PTApplication;
import com.android.chat.peertalk.R;
import com.android.chat.peertalk.entities.PTPeer;
import com.android.chat.peertalk.entities.PTMessage;
import com.android.chat.peertalk.manager.PTChatManager;
import com.android.chat.peertalk.manager.PTUserManager;;
import com.android.chat.peertalk.utils.PTConstants;
import com.android.chat.peertalk.utils.PTUtils;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

public class PTPeerChatActivity extends AppCompatActivity implements PTChatMessageInterface,PTChatPeersInterface {

    private ImageView peerChatSendActionIV;
    private EditText peerChatInputET;
    private ListView peerChatMessagesLV;
    private PTMessagesAdapter messagesAdapter = null;
    private PTUserManager userManager = null;
    private PTChatManager chatManager = null;
    private PTApplication application;
    private long peerId = -1;
    private PTPeer currentPeer = null;
    private boolean isUserTyping = false;
    private Timer timer;
    private long lastTypingTime = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_peer_chat);

        application = (PTApplication)getApplication();
        chatManager = PTChatManager.getInstance(this);
        peerId = getIntent().getLongExtra("peerId",-1);
        if(PTConstants.DUMP_TRACE) {
            Log.d("CHAT","CHAT WITH " + peerId);
        }
        if(peerId == chatManager.currentPeer.peerId) {
            currentPeer = chatManager.currentPeer;
            if(currentPeer != null) {
                getSupportActionBar().setTitle(currentPeer.peerName);

                if(currentPeer.peerPresence == PTConstants.PEER_PRESENCE_ONLINE) {
                    getSupportActionBar().setSubtitle(R.string.peer_presence_online);
                }
                else {
                    getSupportActionBar().setSubtitle(R.string.peer_presence_offline);
                }
            }
        }

        peerChatSendActionIV = (ImageView) findViewById(R.id.peerChatSendActionIV);
        peerChatMessagesLV = (ListView) findViewById(R.id.peerChatMessagesLV);
        peerChatInputET = (EditText) findViewById(R.id.peerChatInputET);
        peerChatInputET.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence cs, int start, int before, int count) {
                onUserTypingStatusChanged(cs);
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void afterTextChanged(Editable arg0) {
            }
        });

        try {
            timer = new Timer();
            timer.schedule(new PromptTimerTask(), 10000, 20000);
        }
        catch (Exception e) {

        }

        userManager = PTUserManager.getInstance(this);
        chatManager = PTChatManager.getInstance(this);
        chatManager.setMessageListener(this);
        chatManager.setPeerListener(this);

        messagesAdapter = new PTMessagesAdapter();
        peerChatMessagesLV.setAdapter(messagesAdapter);

        peerChatSendActionIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(application.isOnline()) {
                    if(!peerChatInputET.getText().toString().isEmpty() && -1 != peerId) {
                        if(currentPeer.peerPresence == PTConstants.PEER_PRESENCE_ONLINE) {
                            PTMessage message = new PTMessage();
                            message.messageCategory = PTConstants.MSG_CAT_SENT;
                            message.messageFromName = userManager.user.userName;
                            message.messageFrom = userManager.user.userId;
                            message.messageTo = peerId;
                            message.messageType = PTConstants.MSG_TYPE_CHAT;
                            message.messageText = peerChatInputET.getText().toString();
                            message.messageTime = PTUtils.getDateTime();
                            addMessage(message);
                        }
                        else if(currentPeer.peerPresence == PTConstants.PEER_PRESENCE_OFFLINE) {
                            Toast.makeText(PTPeerChatActivity.this,R.string.peer_offline_msg,Toast.LENGTH_LONG).show();
                        }
                    }
                }
                else {
                    Toast.makeText(PTPeerChatActivity.this,R.string.online,Toast.LENGTH_LONG).show();
                }
            }
        });

        LoadPeerMessagesAsyncTask task = new LoadPeerMessagesAsyncTask();
        task.execute(peerId);
    }


    public void addMessage(PTMessage message) {
        messagesAdapter.addMessage(message);
        chatManager.sendMessage(message);
        peerChatInputET.setText("");
    }

    public void onGotPeers(ArrayList<PTPeer> peers) {

    }

    public void onPeerJoined(PTPeer peer) {
        if(peer.peerId == currentPeer.peerId) {
            currentPeer.peerPresence = PTConstants.PEER_PRESENCE_ONLINE;
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    getSupportActionBar().setSubtitle(R.string.peer_presence_online);
                }
            });
        }
    }

    public void onPeerLeft(PTPeer peer) {
        if(peer.peerId == currentPeer.peerId) {
            currentPeer.peerPresence = PTConstants.PEER_PRESENCE_OFFLINE;
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    getSupportActionBar().setSubtitle(R.string.peer_presence_offline);
                }
            });
        }
    }

    public void onGotMessage(PTMessage message) {
        if(message.messageType == PTConstants.MSG_TYPE_CHAT && message.messageFrom == peerId) {
            messagesAdapter.addMessage(message);
        }
        else if(message.messageType == PTConstants.MSG_TYPE_STARTED_TYPING) {
            if(message.messageFrom == peerId) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        getSupportActionBar().setSubtitle(R.string.typing);
                    }
                });
            }
        }
        else if(message.messageType == PTConstants.MSG_TYPE_STOPPED_TYPING) {
            if(message.messageFrom == peerId) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        getSupportActionBar().setSubtitle(R.string.peer_presence_online);
                    }
                });
            }
        }
    }

    public void onUserTypingStatusChanged(CharSequence cs) {
        if(cs.length() == 0) {
            if(isUserTyping) {
                isUserTyping = false;
                sendStoppedTypingMsg();
            }
        }
        else if(cs.length() > 0) {
            lastTypingTime = System.currentTimeMillis();
            if(!isUserTyping) {
                isUserTyping = true;
                sendStartedTypingMsg();
            }
        }
    }

    private void sendStartedTypingMsg() {
        PTMessage message = new PTMessage();
        message.messageCategory = PTConstants.MSG_CAT_SENT;
        message.messageFrom = userManager.user.userId;
        message.messageFromName = userManager.user.userName;
        message.messageTo = peerId;
        message.messageType = PTConstants.MSG_TYPE_STARTED_TYPING;
        message.messageText = "";
        message.messageTime = PTUtils.getDateTime();
        chatManager.sendMessage(message);
    }

    private void sendStoppedTypingMsg() {
        PTMessage message = new PTMessage();
        message.messageCategory = PTConstants.MSG_CAT_SENT;
        message.messageFrom = userManager.user.userId;
        message.messageFromName = userManager.user.userName;
        message.messageTo = peerId;
        message.messageType = PTConstants.MSG_TYPE_STOPPED_TYPING;
        message.messageText = "";
        message.messageTime = PTUtils.getDateTime();
        chatManager.sendMessage(message);
    }



    private class MessageViewHolder {
        private RelativeLayout sentMessageRL;
        private TextView sentMessageTV;
        private RelativeLayout receivedMessageRL;
        private TextView receivedMessageTV;

        public MessageViewHolder(View inflatedView) {
            sentMessageRL = (RelativeLayout) inflatedView.findViewById(R.id.sentMessageRL);
            sentMessageTV = (TextView) inflatedView.findViewById(R.id.sentMessageTV);
            receivedMessageRL = (RelativeLayout) inflatedView.findViewById(R.id.receivedMessageRL);
            receivedMessageTV = (TextView) inflatedView.findViewById(R.id.receivedMessageTV);
        }
    }

    private class PTMessagesAdapter extends BaseAdapter {

        private ArrayList<PTMessage> messages = new ArrayList<>();

        @Override
        public int getCount() {
            return messages.size();
        }

        @Override
        public Object getItem(int position) {
            return messages.get(position);
        }

        @Override
        public long getItemId(int id) {
            return id;
        }

        @Override
        public View getView(int position, View view, ViewGroup viewGroup) {

            MessageViewHolder viewHolder = null;

            if(view == null) {
                view = getLayoutInflater().inflate(R.layout.message_row,viewGroup,false);
                viewHolder = new MessageViewHolder(view);
                view.setTag(viewHolder);
            }
            else {
                viewHolder = (MessageViewHolder) view.getTag();
            }

            PTMessage message = messages.get(position);
            if(message.messageCategory == PTConstants.MSG_CAT_SENT) {
                viewHolder.sentMessageRL.setVisibility(View.VISIBLE);
                viewHolder.sentMessageTV.setText(message.messageText);
                viewHolder.receivedMessageRL.setVisibility(View.GONE);
            }
            else {
                viewHolder.sentMessageRL.setVisibility(View.GONE);
                viewHolder.receivedMessageRL.setVisibility(View.VISIBLE);
                viewHolder.receivedMessageTV.setText(message.messageText);
            }

            return view;
        }

        public void updateDataSet(ArrayList<PTMessage> msgs) {
            messages = msgs;
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    notifyDataSetChanged();
                    peerChatMessagesLV.setSelection(messages.size());
                }
            });

        }

        public void addMessage(PTMessage message){
            messages.add(message);
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    notifyDataSetChanged();
                    peerChatMessagesLV.setSelection(messages.size());
                }
            });
        }
    }

    private class LoadPeerMessagesAsyncTask extends AsyncTask<Long,Void,ArrayList<PTMessage>> {

        @Override
        protected ArrayList<PTMessage> doInBackground(Long... params) {
            return chatManager.loadPeerMessages(params[0]);
        }

        @Override
        protected void onPostExecute(ArrayList<PTMessage> messages) {
            messagesAdapter.updateDataSet(messages);
        }
    }

    private class PromptTimerTask extends TimerTask {
        @Override
        public void run() {
            if(lastTypingTime > 0 && (System.currentTimeMillis() - lastTypingTime) > 60000 && isUserTyping) {
                isUserTyping = false;
                sendStoppedTypingMsg();
            }
        }
    }
}
