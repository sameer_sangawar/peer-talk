package com.android.chat.peertalk.manager;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.android.chat.peertalk.utils.PTConstants;

/**
 * Created by sameerss on 25/10/16.
 */

public class PTDBManager extends SQLiteOpenHelper {

    private static final String TAG = "PTDBHelper";
    private static final String DB_PATH = "/data/data/com.android.chat.peertalk/databases/";
    private static final String DB_NAME = "PTDB";
    private static int DB_VERSION = 1;
    private static PTDBManager instance = null;
    private static Context context = null;

    private SQLiteDatabase ptDatabase;

    public boolean isOpen = false;

    public static PTDBManager getInstance(Context ctx) {
        if(instance == null) {
            instance = new PTDBManager(ctx);
        }
        return instance;
    }

    private PTDBManager(Context ctx) {
        super(ctx,DB_NAME,null,DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {

    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }

    private boolean checkDataBase() {
        SQLiteDatabase checkDB = null;

        try {
            String myPath = DB_PATH + DB_NAME;
            checkDB = SQLiteDatabase.openDatabase(myPath, null, SQLiteDatabase.OPEN_READONLY);
        }
        catch (SQLiteException e) {
        }

        if (checkDB != null) {
            checkDB.close();
        }

        return checkDB != null ? true : false;
    }

    public void openDataBase() {

        try {
            isOpen = true;
            String myPath = DB_PATH + DB_NAME;
            ptDatabase = SQLiteDatabase.openDatabase(myPath, null, SQLiteDatabase.OPEN_READWRITE);
        }
        catch (SQLiteException e) {
            Log.e(TAG,"UNABLE TO OPEN DATABASE",e);
            isOpen = false;
        }
    }

    public void createDataBase() {

        boolean dbExist = checkDataBase();

        if (dbExist) {
            return;
        }
        else {
            if (PTConstants.DUMP_TRACE) {
                Log.d(TAG, "Database not exist,Creating New DB");
            }

            // database created and copy
            this.getWritableDatabase();
            openDataBase();
            createOrUpdateDatabaseTables();
            close();
        }
    }

    public void createOrUpdateDatabaseTables() {

        /**
         * TABLE : user
         *
         * user_id INTEGER
         * user_name TEXT
         * user_password TEXT
         * user_initials TEXT
         * user_image_url TEXT
         * user_status TEXT
         * user_presence INTEGER
         */
        try {
            String sql = "CREATE TABLE if not exists user (user_id INTEGER PRIMARY KEY, " +
                    " user_name TEXT, user_password TEXT, user_initials TEXT, user_image_url TEXT, user_status TEXT, " +
                    " user_presence INTEGER)";

            // Create user table
            ptDatabase.execSQL("DROP TABLE if exists user");
            ptDatabase.execSQL(sql);
        }
        catch (SQLException sqle) {
            if(PTConstants.DUMP_TRACE){
                Log.d(TAG, "Could not create user table. Exception : " + sqle.getMessage());
            }
        }

        /**
         * TABLE : peers
         *
         * peer_id INTEGER PRIMARY KEY
         * peer_name TEXT
         * peer_initials TEXT
         * peer_image_url TEXT
         * peer_status TEXT
         * peer_presence INTEGER
         * last_message TEXT
         * last_message_time TEXT
         * last_message_type INTEGER
         * last_message_read_status INTEGER
         * unread_message_count INETGER
         *
         */
        try {
            String sql = "CREATE TABLE if not exists peers (peer_id INTEGER PRIMARY KEY, " +
                    "peer_name TEXT, peer_initials TEXT, peer_image_url TEXT, peer_status TEXT, " +
                    "peer_presence INTEGER, last_message TEXT, last_message_time TEXT, last_message_type INTEGER, " +
                    "last_message_read_status INTEGER, unread_message_count INTEGER)";

            ptDatabase.execSQL("DROP TABLE if exists peers");
            ptDatabase.execSQL(sql);
        }
        catch (Exception e) {
            if(PTConstants.DUMP_TRACE) {
                Log.d(TAG, "Could not create peers table. Exception : " + e.getMessage());
            }
        }

        /**
         *
         * TABLE : messages
         *
         * message_text TEXT
         * message_time TEXT
         * message_type INTEGER
         * message_category INTEGER
         * message_status INTEGER
         * message_from INTEGER
         * message_to INTEGER
         *
         */
        try {
            String sql = "CREATE TABLE if not exists messages (message_text TEXT, message_time TEXT, " +
                    "message_type INTEGER, message_category INTEGER, message_status INTEGER, " +
                    "message_from INTEGER, message_to INTEGER)";

            ptDatabase.execSQL("DROP TABLE if exists messages");
            ptDatabase.execSQL(sql);
        }
        catch (Exception e) {
            if(PTConstants.DUMP_TRACE) {
                Log.d(TAG, "Could not create messages table. Exception : " + e.getMessage());
            }
        }
    }

    public void exec(String sql) {
        if (ptDatabase == null) {
            openDataBase();
        }
        ptDatabase.execSQL(sql);
    }

    public Cursor query(String sql) {
        if (ptDatabase == null) {
            openDataBase();
        }
        Cursor c = ptDatabase.rawQuery(sql, null);
        return c;
    }

    public Long insert(String table, ContentValues values) {
        if (ptDatabase == null) {
            openDataBase();
        }
        return ptDatabase.insertOrThrow(table, null, values);
    }

    public int update(String table, ContentValues values, String whereClause, String[] whereArgs) {
        if (ptDatabase == null) {
            openDataBase();
        }
        return ptDatabase.update(table, values, whereClause, whereArgs);
    }

    public int delete(String table, String whereClause, String[] whereArgs) {
        if (ptDatabase == null) {
            openDataBase();
        }
        return ptDatabase.delete(table, whereClause, whereArgs);
    }


}
