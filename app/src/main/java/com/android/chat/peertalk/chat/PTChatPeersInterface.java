package com.android.chat.peertalk.chat;

import com.android.chat.peertalk.entities.PTPeer;

import java.util.ArrayList;

/**
 * Created by ssangawar on 27/10/2016.
 */

public interface PTChatPeersInterface {
    void onGotPeers(ArrayList<PTPeer> peers);
    void onPeerJoined(PTPeer peer);
    void onPeerLeft(PTPeer peer);
}
