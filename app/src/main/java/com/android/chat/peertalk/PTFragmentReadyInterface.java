package com.android.chat.peertalk;

import android.support.v4.app.Fragment;

/**
 * Created by sameerss on 29/10/16.
 */

public interface PTFragmentReadyInterface {
    void onFragmentReady(Fragment fragment);
}
