package com.android.chat.peertalk.manager;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.os.Looper;
import android.util.Log;

import com.android.chat.peertalk.PTApplication;
import com.android.chat.peertalk.PTRefreshListInterface;
import com.android.chat.peertalk.chat.PTChatMessageInterface;
import com.android.chat.peertalk.chat.PTChatPeersInterface;
import com.android.chat.peertalk.entities.PTPeer;
import com.android.chat.peertalk.entities.PTMessage;
import com.android.chat.peertalk.socketclient.PTWebSocketClient;
import com.android.chat.peertalk.socketclient.PTWebSocketClientInterface;
import com.android.chat.peertalk.utils.PTConstants;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by sameerss on 26/10/16.
 *
 * PTChatManager is a singleton and is responsible for all chat communications.
 * Activities & Fragments, always interact with Chat Manager are never directly
 * with PTWebSocketClient
 *
 * Subscribers can register callback listeners to be notified of events.
 *
 * EVENTS
 *      - PEER_LIST : List of peers when a successful connection is established
 *      - PEER_JOINED : Peer information when a new peer joins the server
 *      - PEER_LEFT : Peer information when a new peer leaves the server
 *      - CHAT : Chat message from/to one of the peers
 *      - STARTED_TYPING : Received when peer starts typing
 *      - STOPPED_TYPING : Received when peer stops typing
 *
 * These events are split into 2 categories
 *      - Peer events which invoke callback methods as declared in PTChatPeersInterface
 *      - Message events which invoke callback methods as declared in PTChatMessageInterface
 */

public class PTChatManager implements PTWebSocketClientInterface {

    private static PTChatManager instance = null;
    private PTDBManager dbManager;
    private PTUserManager userManager;
    private Context context;
    private PTApplication ptApplication;
    private PTWebSocketClient ptWebSocketClient = null;
    private ArrayList<PTChatMessageInterface> messageListeners = new ArrayList<>();
    private ArrayList<PTChatPeersInterface> peerListeners = new ArrayList<>();
    private ArrayList<PTRefreshListInterface> refreshListeners = new ArrayList<>();
    private boolean canChat = false;
    public PTPeer currentPeer = null;
    public HashMap<String ,PTPeer> peers = new HashMap<>();
    public ArrayList<String> peersKeys = new ArrayList<>();

    public static PTChatManager getInstance(Context ctx) {
        if(instance == null) {
            instance = new PTChatManager(ctx);
        }
        return instance;
    }

    private PTChatManager(Context ctx) {
        context = ctx;
        dbManager = PTDBManager.getInstance(ctx);
        userManager = PTUserManager.getInstance(ctx);
    }

    public boolean initialize(PTApplication application){
        ptApplication = application;
        if(ptApplication.isOnline()) {
            ptWebSocketClient = PTWebSocketClient.getInstance(context);
            ptWebSocketClient.setWebSocketListener(this);
            ptWebSocketClient.setPingTimer();
            return true;
        }
        else {
            return false;
        }
    }

    public boolean reConnect(PTApplication application) {
        if(ptWebSocketClient != null) {
            ptWebSocketClient.ptClose();
        }
        if(initialize(application)) {
            connect();
            return true;
        }
        return false;
    }

    public void connect(){
        if(ptWebSocketClient != null) {
            ptWebSocketClient.ptConnect();
        }
    }

    public void close() {
        if(ptWebSocketClient != null) {
            ptWebSocketClient.ptClose();
        }
        peerListeners.clear();
        messageListeners.clear();
    }

    public void setMessageListener(PTChatMessageInterface listener){
        messageListeners.add(listener);
    }

    public void unsetMessageListener(PTChatMessageInterface listener){
        messageListeners.remove(listener);
    }

    public void setPeerListener(PTChatPeersInterface listener) {
        peerListeners.add(listener);
    }

    public void unsetPeerListener(PTChatPeersInterface listener) {
        peerListeners.remove(listener);
    }

    public void setRefreshListListeners(PTRefreshListInterface listener){
        refreshListeners.add(listener);
    }

    public void unsetRefreshListListeners(PTRefreshListInterface listener){
        refreshListeners.remove(listener);
    }


    @Override
    public void onWebSocketOpen() {
        canChat = true;
    }

    @Override
    public void onWebSocketMessage(String rawMessage) {
        if(PTConstants.DUMP_TRACE) {
            Log.d("MSG","RAW MSG => " + rawMessage);
        }

        PTMessage message = PTMessage.build(rawMessage);

        switch (message.messageType) {
            case PTConstants.MSG_TYPE_PEER_LIST:
                userManager.user.userId = message.messageTo;
                userManager.update();
                if(message.peers != null & peerListeners != null && !peerListeners.isEmpty()) {
                    //peers = message.peers;
                    for(int i = 0; i < message.peers.size();i++) {
                        PTPeer ptPeer = message.peers.get(i);
                        peers.put(ptPeer.peerName,ptPeer);
                    }

                    //peerKeys = peers.keySet().toArray(new String[peers.size()]);
                    peersKeys.clear();
                    peersKeys.addAll(new ArrayList<>(peers.keySet()));

                    for(int i = 0; i < peerListeners.size(); i++) {
                        peerListeners.get(i).onGotPeers(message.peers);
                    }

                    for(int i = 0; i < refreshListeners.size(); i++) {
                        refreshListeners.get(i).refreshList();
                    }
                }

                break;

            case PTConstants.MSG_TYPE_PEER_JOINED:
                if(message.peers != null & peerListeners != null && !peerListeners.isEmpty()) {
                    //peers.add(message.peers.get(0));
                    peers.put(message.peers.get(0).peerName,message.peers.get(0));
                    peersKeys.clear();
                    peersKeys.addAll(new ArrayList<>(peers.keySet()));
                    for(int i = 0; i < peerListeners.size(); i++) {
                        peerListeners.get(i).onPeerJoined(message.peers.get(0));
                    }
                    for(int i = 0; i < refreshListeners.size(); i++) {
                        refreshListeners.get(i).refreshList();
                    }
                }
                break;

            case PTConstants.MSG_TYPE_PEER_LEFT:
                if(message.peers != null & peerListeners != null && !peerListeners.isEmpty()) {
                    peers.remove(message.peers.get(0).peerName);
                    peersKeys.clear();
                    peersKeys.addAll(new ArrayList<>(peers.keySet()));

                    for(int i = 0; i < peerListeners.size(); i++) {
                        peerListeners.get(i).onPeerLeft(message.peers.get(0));
                    }
                    for(int i = 0; i < refreshListeners.size(); i++) {
                        refreshListeners.get(i).refreshList();
                    }
                }
                break;

            case PTConstants.MSG_TYPE_CHAT:
                if(messageListeners != null && !messageListeners.isEmpty()) {
                    for(int i = 0; i < messageListeners.size(); i++) {
                        messageListeners.get(i).onGotMessage(message);
                    }
                }
                saveMessage(message);
                break;

            case PTConstants.MSG_TYPE_STARTED_TYPING:
                if(messageListeners != null && !messageListeners.isEmpty()) {
                    for(int i = 0; i < messageListeners.size(); i++) {
                        messageListeners.get(i).onGotMessage(message);
                    }
                }
                break;

            case PTConstants.MSG_TYPE_STOPPED_TYPING:
                if(messageListeners != null && !messageListeners.isEmpty()) {
                    for(int i = 0; i < messageListeners.size(); i++) {
                        messageListeners.get(i).onGotMessage(message);
                    }
                }
                break;

            case PTConstants.MSG_TYPE_CONNECT_ERROR:
                if(messageListeners != null && !messageListeners.isEmpty()) {
                    for(int i = 0; i < messageListeners.size(); i++) {
                        messageListeners.get(i).onGotMessage(message);
                    }
                }
                break;
        }

    }

    @Override
    public void onWebSocketClose(int code, String reason, boolean remote) {
        canChat = false;
    }

    @Override
    public void onWebSocketError(Exception ex) {
        canChat = false;
        if(PTConstants.DUMP_TRACE) {
            Log.e("CHMGR","SOCKET EXCEPTION: ", ex);
        }
        PTMessage message = new PTMessage();
        message.messageType = PTConstants.MSG_TYPE_CONNECT_ERROR;
        if(messageListeners != null && !messageListeners.isEmpty()) {
            for(int i = 0; i < messageListeners.size(); i++) {
                messageListeners.get(i).onGotMessage(message);
            }
        }
    }

    public boolean sendMessage(PTMessage message) {
        boolean status = false;

        if(ptApplication.isOnline() && canChat) {
            ptWebSocketClient.sendMessage(message);
            message.messageStatus = PTConstants.MSG_STATUS_SENT;
            status = true;
        }

        if(message.messageType == PTConstants.MSG_TYPE_CHAT) {
            final PTMessage msg = message;
            new Thread(new Runnable() {
                @Override
                public void run() {
                    saveMessage(msg);
                }
            }).start();
        }

        return status;
    }

    private boolean saveMessage(PTMessage message) {

        ContentValues values = new ContentValues();
        values.put("message_text",message.messageText);
        values.put("message_time",message.messageTime);
        values.put("message_type",message.messageType);
        values.put("message_category",message.messageCategory);
        values.put("message_status",message.messageStatus);
        values.put("message_from",message.messageFrom);
        values.put("message_to",message.messageTo);

        try{
            dbManager.insert("messages",values);

            values.clear();
            values.put("last_message",message.messageText);
            if(message.messageCategory == PTConstants.MSG_CAT_SENT) {
                dbManager.update("peers",values,"peer_id = " + message.messageTo,null);
            }
            else {
                dbManager.update("peers",values,"peer_id = " + message.messageFrom,null);
            }

        }
        catch (SQLException sqle) {
            if(PTConstants.DUMP_TRACE) {
                Log.e("CHATMGR","UNABLE TO ADD MESSAGE",sqle);
            }
            return false;
        }
        return true;
    }

    public ArrayList<PTMessage> loadPeerMessages(long peerId){
        ArrayList<PTMessage> messages = new ArrayList<>();
        String sql = "select * from messages where message_from = " + peerId + " OR message_to = " + peerId + " order by message_time asc";

        Cursor cursor = dbManager.query(sql);
        if(cursor != null && cursor.getCount() > 0) {

            int col_message_text = cursor.getColumnIndex("message_text");
            int col_message_time = cursor.getColumnIndex("message_time");
            int col_message_type = cursor.getColumnIndex("message_type");
            int col_message_category = cursor.getColumnIndex("message_category");
            int col_message_status = cursor.getColumnIndex("message_status");
            int col_message_from = cursor.getColumnIndex("message_from");
            int col_message_to = cursor.getColumnIndex("message_to");

            cursor.moveToFirst();

            do {
                PTMessage message = new PTMessage();
                message.messageText = cursor.getString(col_message_text);
                message.messageTime = cursor.getString(col_message_time);
                message.messageType = cursor.getInt(col_message_type);
                message.messageCategory = cursor.getInt(col_message_category);
                message.messageStatus = cursor.getInt(col_message_status);
                message.messageFrom = cursor.getLong(col_message_from);
                message.messageTo = cursor.getLong(col_message_to);

                messages.add(message);

            }while(cursor.moveToNext());
        }

        return messages;
    }

}
