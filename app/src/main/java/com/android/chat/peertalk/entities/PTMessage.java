package com.android.chat.peertalk.entities;

import com.google.gson.Gson;

import java.util.ArrayList;

/**
 * Created by sameerss on 26/10/16.
 */

public class PTMessage {
    public String messageText = "";
    public String messageTime = "";
    public int messageType;
    public int messageCategory;
    public int messageStatus = -1;
    public String messageFromName = "";
    public long messageFrom;
    public long messageTo;
    public ArrayList<PTPeer> peers;

    public static PTMessage build(String rawMessage){
        Gson gson = new Gson();
        PTMessage message = gson.fromJson(rawMessage,PTMessage.class);
        return message;
    }
}
