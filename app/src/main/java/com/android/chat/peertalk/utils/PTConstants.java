package com.android.chat.peertalk.utils;

/**
 * Created by sameerss on 25/10/16.
 */

public class PTConstants {
    public static final boolean DUMP_TRACE = true;

    public static String PT_WEBSOCKET_CLIENT_URL = "ws://54.255.133.164:9000/chat/join?username=";
    //public static String PT_WEBSOCKET_CLIENT_URL = "ws://192.168.1.34:9000/chat/join?username=";

    public static final int MSG_CAT_SENT = 1;
    public static final int MSG_CAT_RECV = 2;

    public static final int MSG_TYPE_CONNECT_ERROR = -1;
    public static final int MSG_TYPE_PING = 1;
    public static final int MSG_TYPE_PEER_LIST = 2;
    public static final int MSG_TYPE_CHAT = 3;
    public static final int MSG_TYPE_STARTED_TYPING = 4;
    public static final int MSG_TYPE_STOPPED_TYPING = 5;
    public static final int MSG_TYPE_PEER_JOINED = 6;
    public static final int MSG_TYPE_PEER_LEFT = 7;

    public static final int MSG_STATUS_NOT_SENT = -1;
    public static final int MSG_STATUS_SENT = 1;
    public static final int MSG_STATUS_RECEIVED = 2;
    public static final int MSG_STATUS_READ = 3;

    public static final int PEER_PRESENCE_OFFLINE = 0;
    public static final int PEER_PRESENCE_ONLINE = 1;
    public static final int PEER_STATUS_BUSY = 2;
}
