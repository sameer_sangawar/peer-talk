package com.android.chat.peertalk.socketclient;

/**
 * Created by sameerss on 26/10/16.
 */

public interface PTWebSocketClientInterface {
    public void onWebSocketOpen();
    public void onWebSocketMessage(String message);
    public void onWebSocketClose(int code, String reason, boolean remote);
    public void onWebSocketError(Exception ex);
}
