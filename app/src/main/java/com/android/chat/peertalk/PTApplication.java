package com.android.chat.peertalk;

import android.app.Application;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

import com.android.chat.peertalk.utils.PTConstants;

/**
 * Created by sameerss on 26/10/16.
 */

public class PTApplication extends Application {

    public boolean isOnline() {
        boolean connected = false;

        try {
            ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
            connected = networkInfo != null && networkInfo.isAvailable() && networkInfo.isConnected();
        }
        catch (Exception e) {
            if (PTConstants.DUMP_TRACE) {
                Log.d("APP", "Check network connectivity exception : " + e.getMessage());
            }
        }

        return connected;
    }
}
