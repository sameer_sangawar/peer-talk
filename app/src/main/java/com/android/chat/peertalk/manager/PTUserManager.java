package com.android.chat.peertalk.manager;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.util.Log;

import com.android.chat.peertalk.entities.PTUser;
import com.android.chat.peertalk.utils.PTConstants;

/**
 * Created by sameerss on 25/10/16.
 */

public class PTUserManager {

    private static PTUserManager instance = null;
    private Context context;
    private PTDBManager dbManager = null;
    public PTUser user = null;
    private Boolean isLoggedIn = null;

    public static PTUserManager getInstance(Context ctx){
        if(instance == null) {
            instance = new PTUserManager(ctx);
        }
        return instance;
    }

    private PTUserManager(Context ctx) {
        context = ctx;
        dbManager = PTDBManager.getInstance(ctx);
    }

    private PTUser loadUser() {
        PTUser currentUser = null;

        try {
            String sql = "select * from user";
            Cursor cursor = dbManager.query(sql);
            if (cursor != null && cursor.getCount() > 0) {
                cursor.moveToFirst();

                int col_user_id = cursor.getColumnIndex("user_id");
                int col_user_name = cursor.getColumnIndex("user_name");
                int col_user_password = cursor.getColumnIndex("user_password");
                int col_user_initials = cursor.getColumnIndex("user_initials");
                int col_user_image_url = cursor.getColumnIndex("user_image_url");
                int col_user_status = cursor.getColumnIndex("user_status");
                int col_user_presence = cursor.getColumnIndex("user_presence");

                currentUser = new PTUser();
                currentUser.userId = cursor.getLong(col_user_id);
                currentUser.userName = cursor.getString(col_user_name);
                currentUser.userPassword = cursor.getString(col_user_password);
                currentUser.userInitials = cursor.getString(col_user_initials);
                currentUser.userImageUrl = cursor.getString(col_user_image_url);
                currentUser.userStatus = cursor.getString(col_user_status);
                currentUser.userPresence = cursor.getInt(col_user_presence);
            }

            cursor.close();
        }
        catch (SQLException sqle) {
            currentUser = null;
        }

        return currentUser;
    }

    public void update(){

        ContentValues userValues = new ContentValues();
        userValues.put("user_id",user.userId);
        userValues.put("user_name",user.userName);
        userValues.put("user_password",user.userPassword);
        userValues.put("user_initials",user.userInitials);
        userValues.put("user_presence",user.userName);

        dbManager.update("user",userValues,null,null);

    }

    public boolean isLoggedIn(){

        if(isLoggedIn != null) {
            return isLoggedIn;
        }
        else {
            user = loadUser();
            if(user == null) {
                isLoggedIn = false;
            }
            else {
                isLoggedIn = true;
            }
            return isLoggedIn;
        }
    }

    public boolean login(String userName,String password){
        if(!userName.isEmpty()) {

            user = loadUser();
            if(user == null) {
                user = new PTUser();
                user.userName = userName;
                user.userPassword = password;
                user.userPresence = PTConstants.PEER_PRESENCE_OFFLINE;
                user.userId = (long)Math.random();

                String[] parts = userName.split(" ");
                if(parts.length > 1) {
                    user.userInitials = String.valueOf(parts[0].charAt(0)).concat(String.valueOf(parts[1].charAt(0)));
                }
                else {
                    user.userInitials = String.valueOf(parts[0].charAt(0));
                }

                //No user has logged in
                ContentValues userValues = new ContentValues();
                userValues.put("user_id",user.userId);
                userValues.put("user_name",user.userName);
                userValues.put("user_password",user.userPassword);
                userValues.put("user_initials",user.userInitials);
                userValues.put("user_presence",user.userName);

                try {
                    dbManager.insert("user", userValues);
                    isLoggedIn = true;
                }
                catch (SQLException sqle) {
                    user = null;
                }

            }
            else {
                if(user.userName.equals(userName) && user.userPassword.equals(password)) {
                    isLoggedIn = true;
                }
                else {
                    isLoggedIn = false;
                }
            }
        }
        return isLoggedIn;
    }
}
