package com.android.chat.peertalk.login;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.chat.peertalk.PTApplication;
import com.android.chat.peertalk.R;
import com.android.chat.peertalk.manager.PTUserManager;

/**
 * Created by sameerss on 25/10/16.
 */

public class PTLoginFragment extends Fragment {

    private EditText userNameET = null;
    private TextView loginTV = null;
    private PTApplication application;
    private PTUserManager userManager = null;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        userManager = PTUserManager.getInstance(getActivity());

        View view = inflater.inflate(R.layout.fragment_login, container, false);
        userNameET = (EditText) view.findViewById(R.id.userNameET);
        loginTV = (TextView) view.findViewById(R.id.loginTV);

        application = (PTApplication)getActivity().getApplication();

        loginTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(application.isOnline()) {
                    if(userNameET.getText().toString().isEmpty()) {
                        Toast.makeText(getActivity(),R.string.screename_empty,Toast.LENGTH_LONG).show();
                    }
                    else {
                        if(userManager.login(userNameET.getText().toString().trim(),"")) {
                            InputMethodManager inputManager = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                            inputManager.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
                            ((PTLoginInterface)getActivity()).onLoginSuccess();
                        }
                        else {
                            Toast.makeText(getActivity(),R.string.login_failed,Toast.LENGTH_LONG).show();
                        }
                    }
                }
                else {
                    Toast.makeText(getActivity(),R.string.online,Toast.LENGTH_LONG).show();
                }
            }
        });
        return view;
    }

    public interface PTLoginInterface {
        void onLoginSuccess();
    }
}
