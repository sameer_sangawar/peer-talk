package com.android.chat.peertalk.utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

/**
 * Created by sameerss on 26/10/16.
 */

public class PTUtils {

    public static String getDateTime() {
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        dateFormat.setTimeZone(TimeZone.getTimeZone("Australia/Sydney"));
        Date date = new Date();
        return dateFormat.format(date);
    }
}
