package com.android.chat.peertalk.chat;

import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.chat.peertalk.PTFragmentReadyInterface;
import com.android.chat.peertalk.PTRefreshListInterface;
import com.android.chat.peertalk.R;
import com.android.chat.peertalk.entities.PTPeer;
import com.android.chat.peertalk.entities.PTMessage;
import com.android.chat.peertalk.manager.PTChatManager;
import com.android.chat.peertalk.manager.PTPeerManager;
import com.android.chat.peertalk.utils.PTConstants;

import java.util.ArrayList;

/**
 * Created by ssangawar on 26/10/2016.
 */

public class PTChatHistoryFragment extends Fragment implements PTChatMessageInterface, PTRefreshListInterface {

    private RecyclerView historyRV;
    private TextView noHistoryTV;
    private PTHistoryAdapter historyAdapter;
    private PTPeerManager peerManager;
    private PTChatManager chatManager;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_chat_history,container,false);
        historyRV = (RecyclerView) view.findViewById(R.id.historyRV);
        noHistoryTV = (TextView) view.findViewById(R.id.noHistoryTV);
        return view;
    }

    @Override
    public void onActivityCreated(Bundle bundle) {
        super.onActivityCreated(bundle);

        chatManager = PTChatManager.getInstance(getActivity());
        chatManager.setMessageListener(this);
        chatManager.setRefreshListListeners(this);
        peerManager = PTPeerManager.getInstance(getActivity());

        historyRV.setLayoutManager(new LinearLayoutManager(getActivity()));
        historyAdapter = new PTHistoryAdapter();
        historyRV.setAdapter(historyAdapter);

        ((PTFragmentReadyInterface)getActivity()).onFragmentReady(PTChatHistoryFragment.this);

    }

    public void onResume() {
        super.onResume();
        LoadPeersAsyncTask peerHistoryAsyncTask = new LoadPeersAsyncTask();
        peerHistoryAsyncTask.execute();
    }

    public void addPeer(PTPeer peer) {
        if(peerManager != null && historyAdapter.addPeer(peer)) {
            peerManager.addPeer(peer);
        }
    }

    public void onGotMessage(PTMessage message){
        historyAdapter.updatePeer(message);
    }

    public void refreshList(){
        if(getActivity() != null) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    historyAdapter.notifyDataSetChanged();
                }
            });
        }
    }

    private class PTHistoryAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

        private static final int VIEW_TYPE_HISTORY_ROW = 1;
        private ArrayList<PTPeer> peers = new ArrayList<>();
        private int[] colors = new int[5];

        public PTHistoryAdapter() {
            colors[0] = ContextCompat.getColor(getActivity(),R.color.peer_row_initials_bg_1);
            colors[1] = ContextCompat.getColor(getActivity(),R.color.peer_row_initials_bg_2);
            colors[2] = ContextCompat.getColor(getActivity(),R.color.peer_row_initials_bg_3);
            colors[3] = ContextCompat.getColor(getActivity(),R.color.peer_row_initials_bg_4);
            colors[4] = ContextCompat.getColor(getActivity(),R.color.peer_row_initials_bg_5);
        }

        private class PTHistoryViewHolder extends RecyclerView.ViewHolder {
            private ImageView peerIV;
            private TextView peerInitialsTV;
            private TextView peerNameTV;
            private View peerStatusV;
            private TextView peerLastMsgTV;

            private PTHistoryViewHolder(View view) {
                super(view);

                peerIV = (ImageView) view.findViewById(R.id.peerIV);
                peerInitialsTV = (TextView) view.findViewById(R.id.peerInitialsTV);
                peerNameTV = (TextView) view.findViewById(R.id.peerNameTV);
                peerStatusV =  view.findViewById(R.id.peerStatusTV);
                peerLastMsgTV = (TextView) view.findViewById(R.id.peerLastMsgTV);
            }
        }

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            RecyclerView.ViewHolder viewHolder = null;
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());

            switch (viewType) {
                case VIEW_TYPE_HISTORY_ROW:
                    View view = inflater.inflate(R.layout.history_row,parent,false);
                    viewHolder = new PTHistoryViewHolder(view);
                    break;
            }

            return viewHolder;
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
            final PTPeer peer = peers.get(position);
            switch (holder.getItemViewType()) {
                case VIEW_TYPE_HISTORY_ROW:
                    PTHistoryViewHolder peerViewHolder = (PTHistoryViewHolder)holder;
                    peerViewHolder.peerNameTV.setText(peer.peerName);
                    peerViewHolder.peerInitialsTV.setText(peer.peerInitials.toUpperCase());

                    if(chatManager.peers.containsKey(peer.peerName)) {
                        peerViewHolder.peerStatusV.setBackground(ResourcesCompat.getDrawable(getActivity().getResources(),R.drawable.status_online,null));
                        peer.peerPresence = PTConstants.PEER_PRESENCE_ONLINE;
                    }
                    else {
                        peerViewHolder.peerStatusV.setBackground(ResourcesCompat.getDrawable(getActivity().getResources(),R.drawable.status_offline,null));
                        peer.peerPresence = PTConstants.PEER_PRESENCE_OFFLINE;
                    }
                    peerViewHolder.peerLastMsgTV.setText(peer.lastMessage);
                    peerViewHolder.peerInitialsTV.setBackgroundColor(colors[position%colors.length]);
                    break;
            }

            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    chatManager.currentPeer = peer;
                    Intent peerChatIntent = new Intent(getActivity(),PTPeerChatActivity.class);
                    peerChatIntent.putExtra("peerId",peer.peerId);
                    startActivity(peerChatIntent);
                }
            });
        }

        @Override
        public int getItemViewType(int position) {
            return VIEW_TYPE_HISTORY_ROW;
        }

        @Override
        public int getItemCount() {
            return peers.size();
        }

        public void updateDataSet(ArrayList<PTPeer> newPeers) {
            peers = newPeers;
            notifyDataSetChanged();
        }

        public boolean addPeer(PTPeer peer) {

            boolean isNewPeer = true;
            for(int i = 0; i< peers.size(); i++){
                if(peer.peerId == peers.get(i).peerId) {
                    isNewPeer = false;
                    break;
                }
            }
            if(isNewPeer) {
                peers.add(peer);
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        noHistoryTV.setVisibility(View.GONE);
                    }
                });
            }

            if(null != getActivity()) {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        notifyDataSetChanged();
                    }
                });
            }

            return isNewPeer;
        }

        public void updatePeer(PTMessage message){
            boolean isNewPeer = true;
            for(int i = 0; i< peers.size(); i++){
                if(message.messageFrom == peers.get(i).peerId) {
                    if(message.messageType == PTConstants.MSG_TYPE_CHAT) {
                        isNewPeer = false;
                        peers.get(i).lastMessage = message.messageText;
                        if(null != getActivity()) {
                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    notifyDataSetChanged();
                                }
                            });
                        }
                        break;
                    }
                }
            }

            if(isNewPeer && message.messageType == PTConstants.MSG_TYPE_CHAT) {
                PTPeer peer = chatManager.peers.get(message.messageFromName);
                peer.lastMessage = message.messageText;
                addPeer(peer);
                peerManager.addPeer(peer);
            }
        }
    }

    private class LoadPeersAsyncTask extends AsyncTask<Void,Void,ArrayList<PTPeer>> {

        @Override
        protected ArrayList<PTPeer> doInBackground(Void... params) {
            return peerManager.loadPeers();
        }

        @Override
        protected void onPostExecute(ArrayList<PTPeer> peers) {
            historyAdapter.updateDataSet(peers);
            if(peers.isEmpty()) {
                noHistoryTV.setVisibility(View.VISIBLE);
            }
            else {
                noHistoryTV.setVisibility(View.GONE);
            }
        }
    }
}
